# CraftDemoHelm

### Installing CraftDemoApp using Helm Charts!

- Add repo
```
helm repo add craftdemoapp https://gitlab.com/api/v4/projects/32232871/packages/helm/stable
helm repo update
```

- Installing mongodb
```
helm install -n craftdemo-prod --create-namespace craftdemo-db --set auth.rootPassword=secretsecret,auth.username=craftdemosvcacc,auth.password=Pass123,auth.database=test bitnami/mongodb
```

- Install craftdemoapp
```
helm install -n craftdemo-prod --create-namespace craftdemo-backend craftdemoapp --set craftdemo.mongodb.hostName=craftdemo-db-mongodb.craftdemo-prod.svc.cluster.local --set craftdemo.aws.accessId=$AWS_ACCESS_KEY_ID --set craftdemo.aws.accessSecret=$AWS_ACCESS_KEY_SECRET
```

- Uninstall craftdemoapp
```
helm uninstall craftdemoapp -n craftdemo-prod
```

### Configuration

Parameter | Description | Default |
--- | --- | --- | 
craftdemo.aws.accessId | AWS access ID to access s3 bucket | `{}` |
craftdemo.aws.accessSecret | AWS access secret to access s3 bucket | `{}` |
service.type | service type | `ClusterIP` |
craftdemo.mongodb.hosts | mongodb hostnames can be sperated by `,` if replicaset | `127.0.0.1:27017` |
autoscaling.enabled | To deploy HPA based on CPU utilization | `false` |
autoscaling.minReplicas | Min number of replicas to run by HPA | `3` |
autoscaling.maxReplicas | Max number of replicas to run by HPA | `10` |
ingress.enabled | to enable istion ingress | `false` |
ingress.hosts| array of hosts names for ingress | `{*}` |
image.tag | image tag to be used to run craftdemoapp | `latest-main` |
metrics.enabled | express metrics | `true` |


